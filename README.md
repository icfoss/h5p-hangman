H5P Hangman
==========

H5P Hangman is game in which user have to identify a word while every wrong input counts and draws hanging man.

## Screenshots

<img src="https://gitlab.com/icfoss/h5p-hangman/raw/master/screenshots/hangman.png"/>


## License

[GPL v3](LICENSE)


## Credits

This content type is developed under the Media & Learning Lab of [ICFOSS](https://icfoss.in)

